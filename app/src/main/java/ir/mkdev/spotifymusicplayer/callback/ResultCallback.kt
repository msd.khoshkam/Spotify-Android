package ir.mkdev.shootballi.callback

interface ResultCallback {
    fun onResult(result: Any?)
}