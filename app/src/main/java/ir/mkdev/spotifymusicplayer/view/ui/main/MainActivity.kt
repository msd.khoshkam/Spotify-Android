package ir.mkdev.spotifymusicplayer.view.ui.main

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import com.google.android.material.snackbar.Snackbar
import dagger.android.AndroidInjection
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import ir.mkdev.shootballi.callback.ResultCallback
import ir.mkdev.spotifymusicplayer.R
import ir.mkdev.spotifymusicplayer.enums.ListAction
import ir.mkdev.spotifymusicplayer.extension.*
import ir.mkdev.spotifymusicplayer.utils.ItemOffsetDecoration
import ir.mkdev.spotifymusicplayer.utils.VISIBLE_THRESHOLD
import ir.mkdev.spotifymusicplayer.view.adapter.ArtistListAdapter
import ir.mkdev.spotifymusicplayer.view.base.BaseActivity
import ir.mkdev.spotifymusicplayer.view.ui.detail.DetailActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.toolbar
import javax.inject.Inject


class MainActivity : BaseActivity(), SearchView.OnQueryTextListener,
        SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
    }
    private val compositeDisposable = CompositeDisposable()

    private var mRecyclerAdapter: ArtistListAdapter? = null
    private var snackBar: Snackbar? = null
    private var recyclerLoading = false
    private val detailActivityIntent by lazy {
        Intent(this, DetailActivity::class.java)
    }
    private var searchView: SearchView? = null

    override fun initBeforeView() {
        super.initBeforeView()
        AndroidInjection.inject(this)
    }

    override fun getContentViewId(): Int = R.layout.activity_main

    override fun initViews() {
        super.initViews()
        simpleToolbar(toolbar, getString(R.string.app_name), false)
    }

    private fun searchArtist(action: ListAction) {
        viewModel.searchArtist(action)
                .iomain()
                .doOnSubscribe {
                    if (!swipeRefresh.isRefreshing)
                        swipeRefresh.isRefreshing = true
                    recyclerLoading = true
                }
                .doAfterTerminate {
                    if (swipeRefresh.isRefreshing)
                        swipeRefresh.isRefreshing = false
                    recyclerLoading = false
                }
                .logError(this, object : ResultCallback {
                    override fun onResult(result: Any?) {
                        handleErrorMessage(action, result as String)
                    }
                })
                .subscribe({ response ->
                    handleResponse(action, response.size)
                }, {}).addTo(compositeDisposable)
    }

    private fun setupRecyclerView() {
        mRecyclerAdapter = ArtistListAdapter(this, viewModel.artists).apply {
            clickPublisher
                    .iomain()
                    .subscribe { position ->
                        toolbar.collapseActionView()
                        detailActivityIntent.apply {
                            putExtra(DetailActivity.ARTIST_DATA_INTENT, viewModel.artists[position])
                            startActivity(this)
                        }
                    }.addTo(compositeDisposable)
        }

        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = mRecyclerAdapter
            addItemDecoration(ItemOffsetDecoration(this@MainActivity, R.dimen.item_offset))
            hasFixedSize()
        }

        swipeRefresh.setOnRefreshListener(this)
        recyclerView.fadeIn()

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                if (!recyclerLoading && viewModel.artists.size > 0 && linearLayoutManager.itemCount <=
                        linearLayoutManager.findLastVisibleItemPosition() + VISIBLE_THRESHOLD) {

                    if (!viewModel.next.isNullOrEmpty()) {
                        searchArtist(ListAction.ADD_RANGE)
                    }
                }
            }
        })
    }

    private fun handleResponse(action: ListAction, itemSize: Int? = 0) {
        if (mRecyclerAdapter == null) {
            setupRecyclerView()
        } else {
            when (action) {
                ListAction.RESET -> {
                    mRecyclerAdapter?.notifyDataSetChanged()
                }
                ListAction.ADD_RANGE -> {
                    mRecyclerAdapter?.notifyItemRangeInserted(mRecyclerAdapter!!.itemCount, itemSize!!)
                }
                else -> {
                    throw RuntimeException(getString(R.string.invalid_list_action))
                }
            }
        }
    }

    private fun handleErrorMessage(action: ListAction, message: String?) {
        handleResponse(action)
        snackBar = constRoot.snack(
                if (message.isNullOrEmpty()) getString(R.string.ERROR) else message,
                Snackbar.LENGTH_LONG
        ) {
            action(getString(R.string.retry)) {
                searchArtist(ListAction.ADD_RANGE)
            }
        }
    }

    override fun onRefresh() {
        recyclerLoading = false
        searchArtist(ListAction.RESET)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView = (menu.findItem(R.id.search).actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            setOnQueryTextListener(this@MainActivity)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.search -> {
                val transition = AutoTransition()
                transition.duration = resources.getInteger(R.integer.transition_duration).toLong()
                TransitionManager.beginDelayedTransition(toolbar as ViewGroup, transition)
                item.expandActionView()
            }
        }
        return true
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        if (query.isNotEmpty()) {
            viewModel.query = query
            searchArtist(ListAction.RESET)
        }
        return false
    }

    override fun onResume() {
        super.onResume()
        hideKeyboard()
    }

    override fun onQueryTextChange(newText: String) = false

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
