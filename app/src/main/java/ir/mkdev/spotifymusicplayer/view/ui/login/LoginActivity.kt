package ir.mkdev.spotifymusicplayer.view.ui.login

import android.content.Intent
import com.google.android.material.snackbar.Snackbar
import com.pixplicity.easyprefs.library.Prefs
import com.spotify.sdk.android.authentication.AuthenticationClient
import com.spotify.sdk.android.authentication.AuthenticationRequest
import com.spotify.sdk.android.authentication.AuthenticationResponse
import ir.mkdev.spotifymusicplayer.BuildConfig
import ir.mkdev.spotifymusicplayer.R
import ir.mkdev.spotifymusicplayer.extension.delayOnMainThread
import ir.mkdev.spotifymusicplayer.extension.gone
import ir.mkdev.spotifymusicplayer.extension.snack
import ir.mkdev.spotifymusicplayer.extension.visible
import ir.mkdev.spotifymusicplayer.utils.PREFS_EXPIRES_MILLI_SEC
import ir.mkdev.spotifymusicplayer.utils.PREFS_SPOTIFY_TOKEN
import ir.mkdev.spotifymusicplayer.view.base.BaseActivity
import ir.mkdev.spotifymusicplayer.view.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import java.util.concurrent.TimeUnit

class LoginActivity : BaseActivity() {

    private val AUTH_TOKEN_REQUEST_CODE = 1500

    override fun getContentViewId(): Int = R.layout.activity_login

    override fun initViews() {
        super.initViews()
        cvLogin.setOnClickListener {
            onLoginClick()
        }
    }

    private fun onLoginClick() {
        progressLoading.visible()
        val request = AuthenticationRequest.Builder(BuildConfig.SPOTIFY_CLIENT_ID,
                AuthenticationResponse.Type.TOKEN, BuildConfig.SPOTIFY_REDIRECT_URI)
                .setShowDialog(false)
                .build()

        AuthenticationClient.openLoginActivity(this, AUTH_TOKEN_REQUEST_CODE, request)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        progressLoading.gone()
        if (AUTH_TOKEN_REQUEST_CODE == requestCode) {
            val response = AuthenticationClient.getResponse(resultCode, data)
            response.accessToken?.let {
                Prefs.putString(PREFS_SPOTIFY_TOKEN, it)
                Prefs.putLong(PREFS_EXPIRES_MILLI_SEC, System.currentTimeMillis() +
                        TimeUnit.SECONDS.toMillis(response.expiresIn.toLong()))
                delayOnMainThread({
                    startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                    finish()
                }, 500, TimeUnit.MILLISECONDS)
            } ?: run {
                constRoot.snack(getString(R.string.login_error), Snackbar.LENGTH_LONG) {}
            }
        }
    }
}
