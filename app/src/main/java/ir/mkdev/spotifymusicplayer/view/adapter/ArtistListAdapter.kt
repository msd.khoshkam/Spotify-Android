package ir.mkdev.spotifymusicplayer.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.processors.PublishProcessor
import ir.mkdev.spotifymusicplayer.R
import ir.mkdev.spotifymusicplayer.enums.ItemViewType
import ir.mkdev.spotifymusicplayer.models.network.search.Artist
import ir.mkdev.spotifymusicplayer.view.viewholder.ArtistListViewHolder

class ArtistListAdapter(
        private val context: Context,
        private val items: MutableList<Artist>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var layoutInflater: LayoutInflater = LayoutInflater.from(context)
    val clickPublisher: PublishProcessor<Int> = PublishProcessor.create()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ItemViewType.ITEM.ordinal) {
            ArtistListViewHolder(layoutInflater.inflate(R.layout.item_recycler_artist,
                    parent, false), clickPublisher)
        } else {
            EmptyViewHolder(layoutInflater.inflate(R.layout.item_recycler_empty, parent, false))
        }
    }

    override fun getItemCount() = if (items.isEmpty()) 1 else items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ArtistListViewHolder -> {
                holder.bindView(items[position], position)
            }
            is EmptyViewHolder -> {
                holder.bindView(title = context.getString(R.string.no_results_found))
            }
        }
    }

    override fun getItemViewType(position: Int): Int =
            if (items.isEmpty()) ItemViewType.EMPTY.ordinal else ItemViewType.ITEM.ordinal
}