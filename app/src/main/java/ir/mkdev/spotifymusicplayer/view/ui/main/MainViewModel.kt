package ir.mkdev.spotifymusicplayer.view.ui.main

import androidx.lifecycle.ViewModel
import io.reactivex.Single
import ir.mkdev.spotifymusicplayer.api.SpotifyService
import ir.mkdev.spotifymusicplayer.enums.ListAction
import ir.mkdev.spotifymusicplayer.models.network.search.Artist
import timber.log.Timber
import javax.inject.Inject

class MainViewModel
@Inject constructor(private val service: SpotifyService) : ViewModel() {

    val artists: MutableList<Artist> = mutableListOf()
    var query: String = ""

    var limit: Int = 20
    var offset: Int = 0
    var next: String? = "-1"

    init {
        Timber.d("injection ${javaClass.simpleName}")
    }

    fun searchArtist(action: ListAction): Single<List<Artist>> {
        return service.searchArtist(
                query,
                "artist",
                "ES",
                limit,
                if (action == ListAction.RESET) 0 else offset)
                .doOnSuccess {
                    if (action == ListAction.RESET) {
                        artists.clear()
                        offset = 0
                        next = "-1"
                    }

                    offset += limit
                    next = it.artists.next
                    artists.addAll(it.artists.items)
                }.map { it.artists.items }
    }
}