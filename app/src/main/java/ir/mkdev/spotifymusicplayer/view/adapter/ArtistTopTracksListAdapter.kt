package ir.mkdev.spotifymusicplayer.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.mkdev.spotifymusicplayer.R
import ir.mkdev.spotifymusicplayer.models.network.topTracks.Track
import ir.mkdev.spotifymusicplayer.view.viewholder.ArtistTopTracksListViewHolder

class ArtistTopTracksListAdapter(
        private val context: Context,
        private val items: MutableList<Track>
) : RecyclerView.Adapter<ArtistTopTracksListViewHolder>() {

    private var layoutInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArtistTopTracksListViewHolder {
        return ArtistTopTracksListViewHolder(layoutInflater.inflate(R.layout.item_recycler_top_tracks,
                        parent, false))
    }

    override fun getItemCount() = if (items.isEmpty()) 1 else items.size

    override fun onBindViewHolder(holder: ArtistTopTracksListViewHolder, position: Int) {
        holder.bindView(items[position], position)
    }
}