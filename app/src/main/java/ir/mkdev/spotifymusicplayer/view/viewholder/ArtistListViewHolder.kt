package ir.mkdev.spotifymusicplayer.view.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.GenericTransitionOptions
import io.reactivex.processors.PublishProcessor
import ir.mkdev.spotifymusicplayer.R
import ir.mkdev.spotifymusicplayer.extension.customRequest
import ir.mkdev.spotifymusicplayer.extension.setCustomAnimation
import ir.mkdev.spotifymusicplayer.models.network.search.Artist
import ir.mkdev.spotifymusicplayer.utils.GlideApp
import kotlinx.android.synthetic.main.item_recycler_artist.view.*

class ArtistListViewHolder(itemView: View,
                           clickPublisher: PublishProcessor<Int>) :
        RecyclerView.ViewHolder(itemView) {

    private var lastPosition = -1

    init {
        itemView.constRoot.setOnClickListener {
            clickPublisher.onNext(adapterPosition)
        }
    }

    fun bindView(item: Artist, position: Int) {
        itemView.run {
            GlideApp.with(context)
                    .load(item.getThumbnailImage() ?: "")
                    .transition(GenericTransitionOptions.with(R.anim.fade_in))
                    .customRequest(R.drawable.default_album_art, R.drawable.default_album_art)
                    .into(ivArtistImage)

            tvArtistName.text = item.name
            tvFollowers.text = item.followers?.total?.toString() ?: "0"
            tvPopularity.text = item.popularity?.toString() ?: "0"
        }

        itemView.setCustomAnimation(position, lastPosition)
        lastPosition = position
    }
}