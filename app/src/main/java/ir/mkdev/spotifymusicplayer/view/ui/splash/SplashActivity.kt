package ir.mkdev.spotifymusicplayer.view.ui.splash

import android.content.Intent
import android.os.Bundle
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import ir.mkdev.spotifymusicplayer.R
import ir.mkdev.spotifymusicplayer.utils.PREFS_EXPIRES_MILLI_SEC
import ir.mkdev.spotifymusicplayer.utils.PREFS_SPOTIFY_TOKEN
import ir.mkdev.spotifymusicplayer.view.base.BaseActivity
import ir.mkdev.spotifymusicplayer.view.ui.login.LoginActivity
import ir.mkdev.spotifymusicplayer.view.ui.main.MainActivity
import java.util.concurrent.TimeUnit


class SplashActivity : BaseActivity() {

    private var disposable : Disposable? = null

    override fun getContentViewId(): Int = R.layout.activity_splash

    override fun initViews() {
        super.initViews()

        disposable = Completable.timer(2, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .doOnComplete { }
                .subscribe({
                    val currentTime = System.currentTimeMillis()
                    val expireTime = Prefs.getLong(PREFS_EXPIRES_MILLI_SEC, 0)
                    val token = Prefs.getString(PREFS_SPOTIFY_TOKEN, null)
                    if (currentTime >= expireTime || token.isNullOrEmpty()) {
                        startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
                    } else {
                        startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                    }
                    finish()
                }, {})
    }

    override fun onDestroy() {
        disposable?.dispose()
        super.onDestroy()
    }

    override fun onBackPressed() {}
}
