package ir.mkdev.spotifymusicplayer.view.ui.detail

import androidx.lifecycle.ViewModel
import io.reactivex.Single
import ir.mkdev.spotifymusicplayer.api.SpotifyService
import ir.mkdev.spotifymusicplayer.models.network.search.Artist
import ir.mkdev.spotifymusicplayer.models.network.topTracks.Track
import timber.log.Timber
import javax.inject.Inject

class DetailViewModel
@Inject constructor(private val service: SpotifyService) : ViewModel() {

    var artist: Artist = Artist()
    var tracks: MutableList<Track> = mutableListOf()

    init {
        Timber.d("injection ${javaClass.simpleName}")
    }

    fun getTopTracks(): Single<List<Track>> {
        return service.getTopTracks(artist.id, "ES")
                .map { it.tracks }
                .doOnSuccess {
                    tracks.addAll(it)
                }
    }
}