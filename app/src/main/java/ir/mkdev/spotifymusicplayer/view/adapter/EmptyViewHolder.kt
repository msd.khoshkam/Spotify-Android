package ir.mkdev.spotifymusicplayer.view.adapter

import android.view.View
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.RecyclerView
import ir.mkdev.spotifymusicplayer.R
import ir.mkdev.spotifymusicplayer.extension.gone
import ir.mkdev.spotifymusicplayer.extension.visible
import kotlinx.android.synthetic.main.item_recycler_empty.view.*

class EmptyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

    fun bindView(@DrawableRes image: Int = R.drawable.ic_launcher_foreground, title: String = "") {
        itemView.run {
            ivImage?.setImageResource(image)
            tvTitle?.text = title.trim()

            if (title.isNotEmpty()) {
                tvTitle.visible()
            } else {
                tvTitle.gone()
            }
        }
    }
}