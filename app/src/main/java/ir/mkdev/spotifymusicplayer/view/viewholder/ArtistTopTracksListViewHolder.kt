package ir.mkdev.spotifymusicplayer.view.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.GenericTransitionOptions
import ir.mkdev.spotifymusicplayer.R
import ir.mkdev.spotifymusicplayer.extension.customRequest
import ir.mkdev.spotifymusicplayer.extension.setCustomAnimation
import ir.mkdev.spotifymusicplayer.models.network.topTracks.Track
import ir.mkdev.spotifymusicplayer.utils.GlideApp
import kotlinx.android.synthetic.main.item_recycler_top_tracks.view.*

class ArtistTopTracksListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var lastPosition = -1

    fun bindView(item: Track, position: Int) {

        itemView.run {
            GlideApp.with(context)
                    .load(item.album?.getThumbnailImage() ?: "")
                    .transition(GenericTransitionOptions.with(R.anim.fade_in))
                    .customRequest(R.drawable.default_album_art, R.drawable.default_album_art)
                    .into(ivTrackImage)

            tvTrackName.text = item.name
            tvAlbumName.text = item.album?.name ?: context.getString(R.string.none)
        }

        itemView.setCustomAnimation(position, lastPosition)
        lastPosition = position
    }
}