package ir.mkdev.spotifymusicplayer.view.ui.detail

import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.GenericTransitionOptions
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.chip.Chip
import dagger.android.AndroidInjection
import io.reactivex.disposables.Disposable
import ir.mkdev.spotifymusicplayer.R
import ir.mkdev.spotifymusicplayer.extension.*
import ir.mkdev.spotifymusicplayer.models.network.search.Artist
import ir.mkdev.spotifymusicplayer.utils.GlideApp
import ir.mkdev.spotifymusicplayer.utils.ItemOffsetDecoration
import ir.mkdev.spotifymusicplayer.view.adapter.ArtistTopTracksListAdapter
import ir.mkdev.spotifymusicplayer.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.partial_details_info.*
import javax.inject.Inject

class DetailActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(DetailViewModel::class.java)
    }
    private var disposable: Disposable? = null

    override fun initBeforeView() {
        super.initBeforeView()
        AndroidInjection.inject(this)

        intent?.let {
            if (it.hasExtra(ARTIST_DATA_INTENT)) {
                viewModel.artist = it.getParcelableExtra(ARTIST_DATA_INTENT) as Artist
            }
        }
    }

    override fun getContentViewId(): Int = R.layout.activity_detail

    override fun initViews() {
        super.initViews()
        simpleToolbar(toolbar, viewModel.artist.name ?: getString(R.string.none), withHome = true)
        handleCollapsedToolbarTitle()

        getTopTracks()

        viewModel.artist.also {
            GlideApp.with(this@DetailActivity)
                    .load(it.getLargeImage() ?: "")
                    .listener(requestGlideListener(ivArtistLargeBackdrop))
                    .customRequest(R.drawable.default_album_art, R.drawable.default_album_art)
                    .into(ivArtistLargeBackdrop)

            GlideApp.with(this@DetailActivity)
                    .load(it.getLargeImage())
                    .transition(GenericTransitionOptions.with(R.anim.fade_in))
                    .customRequest(R.drawable.default_album_art, R.drawable.default_album_art)
                    .into(ivPoster)

            tvTitle.text = it.name ?: getString(R.string.none)
            tvFollowers.text = it.followers?.total?.toString() ?: "0"
            tvType.text = it.type ?: getString(R.string.none)
            tvPopularity.text = it.popularity?.toString() ?: "0"

            var chip: Chip
            it.getGenresList()?.forEach { genres ->
                chip = Chip(chipGroup.context)
                chip.text = genres.toUpperCase()
                chipGroup.addView(chip)
            }
        }
    }

    /**
     * sets the title on the toolbar only if the toolbar is collapsed
     */
    private fun handleCollapsedToolbarTitle() {
        appBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            var isShow = true
            var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                // verify if the toolbar is completely collapsed and set the movie name as the title
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.title = viewModel.artist.name ?: " "
                    isShow = true
                } else if (isShow) {
                    // display an empty string when toolbar is expanded
                    collapsingToolbarLayout.title = " "
                    isShow = false
                }
            }
        })
    }

    private fun getTopTracks() {
        disposable = viewModel.getTopTracks()
                .iomain()
                .subscribe({
                    setupRecyclerView()
                }, {})
    }

    private fun setupRecyclerView() {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = ArtistTopTracksListAdapter(this@DetailActivity, viewModel.tracks)
            addItemDecoration(ItemOffsetDecoration(this@DetailActivity, R.dimen.item_offset))
            hasFixedSize()
            isNestedScrollingEnabled = false
        }

        groupTopAlbums.visible()
    }

    companion object {
        const val ARTIST_DATA_INTENT = "artist_data"
    }

    override fun onDestroy() {
        disposable?.dispose()
        super.onDestroy()
    }
}
