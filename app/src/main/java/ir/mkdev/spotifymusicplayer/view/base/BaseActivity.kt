package ir.mkdev.spotifymusicplayer.view.base

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        initBeforeView()
        setContentView(getContentViewId())
        initViews()
    }

    protected open fun initBeforeView(){}

    protected abstract fun getContentViewId(): Int

    protected open fun initViews(){}
}