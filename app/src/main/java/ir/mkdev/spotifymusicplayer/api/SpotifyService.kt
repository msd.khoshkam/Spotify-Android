package ir.mkdev.spotifymusicplayer.api

import io.reactivex.Single
import ir.mkdev.spotifymusicplayer.models.network.search.SearchModel
import ir.mkdev.spotifymusicplayer.models.network.topTracks.ArtistTopTracksModel
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SpotifyService {

    /**
     * https://api.spotify.com/v1/search
     */
    @GET("search")
    fun searchArtist(
            @Query("q") query: String,
            @Query("type") type: String,
            @Query("market") market: String,
            @Query("limit") limit: Int,
            @Query("offset") offset: Int
    ): Single<SearchModel>

    /**
     * https://api.spotify.com/v1/artists/{artistId}/top-tracks
     */
    @GET("artists/{artistId}/top-tracks")
    fun getTopTracks(
            @Path("artistId") artistId: String,
            @Query("country") country: String
    ): Single<ArtistTopTracksModel>
}
