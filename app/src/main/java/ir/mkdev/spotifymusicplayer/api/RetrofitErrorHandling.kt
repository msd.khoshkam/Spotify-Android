package ir.mkdev.spotifymusicplayer.api

import android.accounts.NetworkErrorException
import android.content.Context
import com.google.gson.Gson
import ir.mkdev.spotifymusicplayer.R
import ir.mkdev.spotifymusicplayer.extension.isNetworkAvailable
import ir.mkdev.spotifymusicplayer.models.network.ErrorHandlingModel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Response
import timber.log.Timber
import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException


fun getRetrofitErrorMessage(context: Context, error: Any): Any? {

    Timber.e("Network error :  $error")
    return if (!context.isNetworkAvailable()) {
        makeErrorObject(context.getString(R.string.no_connect_to_internet), 504)
    } else if (error is SocketTimeoutException) {
        makeErrorObject(context.getString(R.string.SERVER_TIMEOUT), 408)
    } else if (isNetworkProblem(error)) {
        makeErrorObject(context.getString(R.string.no_connect_to_server), 204)
    } else if (error is HttpException) {
        handleServerError(context, error)
    } else if (error is IOException) {
        makeErrorObject(context.getString(R.string.SERVER_ERROR), 500)
    } else {
        makeErrorObject(context.getString(R.string.SERVER_ERROR), 500)
    }
}

private fun makeErrorObject(message: String?,
                            code: Int,
                            status: Boolean = false): ErrorHandlingModel {
    return ErrorHandlingModel().apply {
        this.message = message
        this.errorCode = code
        this.status = status
    }
}

private fun isNetworkProblem(error: Any): Boolean {
    return error is ConnectException || error is NetworkErrorException
}

private fun handleServerError(context: Context, error: Any): Any? {
    val response: Response<*>

    try {
        val httpException = error as HttpException
        response = httpException.response()
        Timber.e(error.toString())
    } catch (e: ClassCastException) {
        return makeErrorObject(context.getString(R.string.ERROR), 500)
    }

    val errorResponse = ErrorHandlingModel()
    try {
        val mainObject = JSONObject((response.errorBody() as ResponseBody).string())
        val errorObject = mainObject.getJSONObject("error")
        errorResponse.errorCode = errorObject.getInt("status")
        errorResponse.message = errorObject.getString("message")
        errorResponse.status = false
    } catch (ex: Exception) {
        ex.printStackTrace()
    }

    return when (response.code()) {
        400 -> errorResponse
        403 -> {// user invalid
            errorResponse
        }
        404 -> {// server error
            errorResponse
        }
        405, 500 -> {
            errorResponse
        }
        401 -> errorResponse
        422 -> errorResponse
        else -> {
            makeErrorObject(context.getString(R.string.no_connect_to_server), 500)
        }
    }
}
