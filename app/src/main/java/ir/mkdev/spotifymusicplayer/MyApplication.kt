package ir.mkdev.spotifymusicplayer

import com.bumptech.glide.Glide
import com.pixplicity.easyprefs.library.Prefs
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import ir.mkdev.spotifymusicplayer.di.DaggerAppComponent
import timber.log.Timber

class MyApplication : DaggerApplication() {

    private val appComponent = DaggerAppComponent.builder()
            .application(this)
            .build()

    override fun onLowMemory() {
        try {
            Thread(Runnable {
                Glide.get(this).clearDiskCache()
            }).start()
        } catch (e: IllegalArgumentException) {
            Timber.e(e)
        }
        super.onLowMemory()
    }

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
        Prefs.Builder().setContext(this).setUseDefaultSharedPreference(true).build()
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = appComponent
}

