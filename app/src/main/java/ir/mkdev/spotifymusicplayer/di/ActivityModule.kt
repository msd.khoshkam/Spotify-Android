package ir.mkdev.spotifymusicplayer.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ir.mkdev.spotifymusicplayer.view.ui.detail.DetailActivity
import ir.mkdev.spotifymusicplayer.view.ui.login.LoginActivity
import ir.mkdev.spotifymusicplayer.view.ui.main.MainActivity

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    internal abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun contributeDetailActivity(): DetailActivity

    @ContributesAndroidInjector
    internal abstract fun contributeLoginActivity(): LoginActivity
}
