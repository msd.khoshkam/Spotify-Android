package ir.mkdev.spotifymusicplayer.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
internal class AppModule {

    @Provides
    @Singleton
    fun provideContext(app: Application): Context = app.applicationContext
}
