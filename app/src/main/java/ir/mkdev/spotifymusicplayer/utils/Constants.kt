package ir.mkdev.spotifymusicplayer.utils

const val API_BASE_URL = "https://api.spotify.com/v1/"
const val PREFS_SPOTIFY_TOKEN = "spotify_token"
const val PREFS_EXPIRES_MILLI_SEC = "expires_milli_sec"
const val VISIBLE_THRESHOLD = 5
