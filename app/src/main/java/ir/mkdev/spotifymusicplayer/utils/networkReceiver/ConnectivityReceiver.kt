package ir.mkdev.spotifymusicplayer.utils.networkReceiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import ir.mkdev.spotifymusicplayer.extension.isNetworkAvailable

class ConnectivityReceiver constructor(
        private val mConnectivityReceiverListener: ConnectivityReceiverListener
) : BroadcastReceiver() {


    override fun onReceive(context: Context, intent: Intent) {
        mConnectivityReceiverListener.onNetworkConnectionChanged(context.isNetworkAvailable())
    }

    interface ConnectivityReceiverListener {
        fun onNetworkConnectionChanged(isConnected: Boolean)
    }
}