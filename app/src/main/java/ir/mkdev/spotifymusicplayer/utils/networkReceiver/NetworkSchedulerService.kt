package ir.mkdev.spotifymusicplayer.utils.networkReceiver

import android.app.Service
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Intent
import android.content.IntentFilter
import ir.mkdev.spotifymusicplayer.extension.toast
import timber.log.Timber

class NetworkSchedulerService : JobService(), ConnectivityReceiver.ConnectivityReceiverListener {

    private val tag = NetworkSchedulerService::class.java.simpleName
    private var mConnectivityReceiver: ConnectivityReceiver? = null
    private val connectivityAction = "android.net.conn.CONNECTIVITY_CHANGE"

    override fun onCreate() {
        super.onCreate()
        Timber.i(tag, "Service created")
        mConnectivityReceiver = ConnectivityReceiver(this)
    }

    /**
     * When the app's NetworkConnectionActivity is created, it starts this service. This is so that the
     * activity and this service can communicate back and forth. See "setUiCallback()"
     */
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Timber.i(tag, "onStartCommand")
        return Service.START_NOT_STICKY
    }

    override fun onStartJob(p0: JobParameters?): Boolean {
        Timber.i(tag, "onStartJob %s", mConnectivityReceiver)
        registerReceiver(mConnectivityReceiver, IntentFilter(connectivityAction))
        return true
    }

    override fun onStopJob(p0: JobParameters?): Boolean {
        Timber.i(tag, "onStopJob")
        unregisterReceiver(mConnectivityReceiver)
        return true
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        val message =
                if (isConnected)
                    "Good! Connected to Internet"
                else
                    "Sorry! Not connected to internet"
        toast(message)
    }
}