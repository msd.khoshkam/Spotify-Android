package ir.mkdev.spotifymusicplayer.models.network.search


import com.google.gson.annotations.SerializedName

data class SearchModel(
        @SerializedName("artists")
        var artists: Artists = Artists()
)