package ir.mkdev.spotifymusicplayer.models.network.search


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Artist(
        @SerializedName("followers")
        var followers: Followers? = null,
        @SerializedName("genres")
        var genres: List<String>? = listOf(),
        @SerializedName("href")
        var href: String? = "",
        @SerializedName("id")
        var id: String = "",
        @SerializedName("images")
        var images: List<Image>? = listOf(),
        @SerializedName("name")
        var name: String? = null,
        @SerializedName("popularity")
        var popularity: Int? = null,
        @SerializedName("type")
        var type: String? = null,
        @SerializedName("uri")
        var uri: String? = ""
) : Parcelable {
    fun getThumbnailImage(): String? {
        return if (!images.isNullOrEmpty()) images?.reversed()?.first()?.url else ""
    }

    fun getLargeImage(): String? {
        return if (!images.isNullOrEmpty()) images?.first()?.url else ""
    }

    fun getGenresList(): List<String>? {
        return if (!genres.isNullOrEmpty()) genres?.first()?.split(" ") else null
    }

    constructor(source: Parcel) : this(
            source.readParcelable<Followers>(Followers::class.java.classLoader),
            source.createStringArrayList(),
            source.readString(),
            source.readString()!!,
            source.createTypedArrayList(Image.CREATOR),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeParcelable(followers, 0)
        writeStringList(genres)
        writeString(href)
        writeString(id)
        writeTypedList(images)
        writeString(name)
        writeValue(popularity)
        writeString(type)
        writeString(uri)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Artist> = object : Parcelable.Creator<Artist> {
            override fun createFromParcel(source: Parcel): Artist = Artist(source)
            override fun newArray(size: Int): Array<Artist?> = arrayOfNulls(size)
        }
    }
}