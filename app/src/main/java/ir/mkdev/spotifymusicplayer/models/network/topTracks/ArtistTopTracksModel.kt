package ir.mkdev.spotifymusicplayer.models.network.topTracks


import com.google.gson.annotations.SerializedName

data class ArtistTopTracksModel(
    @SerializedName("tracks")
    var tracks: List<Track> = listOf()
)