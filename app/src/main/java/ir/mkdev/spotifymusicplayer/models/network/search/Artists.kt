package ir.mkdev.spotifymusicplayer.models.network.search


import com.google.gson.annotations.SerializedName

data class Artists(
        @SerializedName("href")
        var href: String? = "",
        @SerializedName("items")
        var items: List<Artist> = listOf(),
        @SerializedName("limit")
        var limit: Int? = 0,
        @SerializedName("next")
        var next: String? = "",
        @SerializedName("offset")
        var offset: Int = 0,
        @SerializedName("total")
        var total: Int = 0
)