package ir.mkdev.spotifymusicplayer.models.network.topTracks


import com.google.gson.annotations.SerializedName
import ir.mkdev.spotifymusicplayer.models.network.search.Artist
import ir.mkdev.spotifymusicplayer.models.network.search.Image

data class Album(
        @SerializedName("album_type")
        var albumType: String? = "",
        @SerializedName("artists")
        var artists: List<Artist?>? = listOf(),
        @SerializedName("href")
        var href: String? = "",
        @SerializedName("id")
        var id: String? = "",
        @SerializedName("images")
        var images: List<Image?>? = listOf(),
        @SerializedName("name")
        var name: String? = "",
        @SerializedName("release_date")
        var releaseDate: String? = "",
        @SerializedName("release_date_precision")
        var releaseDatePrecision: String? = "",
        @SerializedName("total_tracks")
        var totalTracks: Int? = 0,
        @SerializedName("type")
        var type: String? = "",
        @SerializedName("uri")
        var uri: String? = ""
) {
    fun getThumbnailImage(): String? {
        return if (!images.isNullOrEmpty()) images?.reversed()?.first()?.url else ""
    }

    fun getLargeImage(): String? {
        return if (!images.isNullOrEmpty()) images?.first()?.url else ""
    }
}