package ir.mkdev.spotifymusicplayer.models.network.search


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Followers(
        @SerializedName("total")
        var total: Int? = null
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readValue(Int::class.java.classLoader) as Int?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(total)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Followers> = object : Parcelable.Creator<Followers> {
            override fun createFromParcel(source: Parcel): Followers = Followers(source)
            override fun newArray(size: Int): Array<Followers?> = arrayOfNulls(size)
        }
    }
}