package ir.mkdev.spotifymusicplayer.models.network.topTracks


import com.google.gson.annotations.SerializedName
import ir.mkdev.spotifymusicplayer.models.network.search.Artist

data class Track(
        @SerializedName("album")
        var album: Album? = Album(),
        @SerializedName("artists")
        var artists: List<Artist>? = null,
        @SerializedName("disc_number")
        var discNumber: Int? = 0,
        @SerializedName("duration_ms")
        var durationMs: Int? = 0,
        @SerializedName("explicit")
        var explicit: Boolean? = false,
        @SerializedName("href")
        var href: String? = "",
        @SerializedName("id")
        var id: String? = "",
        @SerializedName("is_local")
        var isLocal: Boolean? = false,
        @SerializedName("is_playable")
        var isPlayable: Boolean? = false,
        @SerializedName("name")
        var name: String? = "",
        @SerializedName("popularity")
        var popularity: Int? = 0,
        @SerializedName("preview_url")
        var previewUrl: String? = "",
        @SerializedName("track_number")
        var trackNumber: Int? = 0,
        @SerializedName("type")
        var type: String? = "",
        @SerializedName("uri")
        var uri: String? = ""
)