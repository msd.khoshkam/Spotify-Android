package ir.mkdev.spotifymusicplayer.models.network

import com.google.gson.annotations.SerializedName

class ErrorHandlingModel {
    @SerializedName("success")
    var status: Boolean = true
    @SerializedName("error_code")
    var errorCode: Int? = 200
    @SerializedName("message")
    var message: String? = ""
}
