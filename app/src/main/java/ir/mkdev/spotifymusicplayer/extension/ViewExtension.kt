package ir.mkdev.spotifymusicplayer.extension

import android.app.Activity
import android.content.Context
import android.util.DisplayMetrics
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import ir.mkdev.spotifymusicplayer.R

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.inVisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

inline fun <reified T> RecyclerView.ViewHolder.dpToPx(dp: Int): T {
    return when (T::class) {
        Float::class -> (dp * itemView.resources.displayMetrics.density) as T
        Int::class -> (dp * itemView.resources.displayMetrics.density).toInt() as T
        else -> throw RuntimeException("invalid return type")
    }
}

inline fun <reified T> Fragment.dpToPx(dp: Int): T {
    return when (T::class) {
        Float::class -> (dp * context!!.resources.displayMetrics.density) as T
        Int::class -> (dp * context!!.resources.displayMetrics.density).toInt() as T
        else -> throw RuntimeException("invalid return type")
    }
}

inline fun <reified T> View.dpToPx(dp: Int): T {
    return when (T::class) {
        Float::class -> (dp * resources.displayMetrics.density) as T
        Int::class -> (dp * resources.displayMetrics.density).toInt() as T
        else -> throw RuntimeException("invalid return type")
    }
}

inline fun <reified T> Context.dpToPx(dp: Int): T {
    return when (T::class) {
        Float::class -> (dp * resources.displayMetrics.density) as T
        Int::class -> (dp * resources.displayMetrics.density).toInt() as T
        else -> throw RuntimeException("invalid return type")
    }
}

fun View.fadeIn() {
    if (alpha == 1f) {
        this.alpha = 0.99f
        val fadeIn = AnimationUtils.loadAnimation(context, R.anim.fade_in)
        this.startAnimation(fadeIn)
        fadeIn.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(p0: Animation?) = Unit
            override fun onAnimationStart(p0: Animation?) = Unit
            override fun onAnimationEnd(p0: Animation?) {
                alpha = 1f
                visible()
            }
        })
    }
}

fun View.fadeOut() {
    if (alpha == 1f) {
        this.alpha = 0.99f
        val fadeOut = AnimationUtils.loadAnimation(context, R.anim.fade_out)
        this.startAnimation(fadeOut)
        fadeOut.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(p0: Animation?) = Unit
            override fun onAnimationStart(p0: Animation?) = Unit
            override fun onAnimationEnd(p0: Animation?) {
                alpha = 1f
                gone()
            }
        })
    }
}

fun Activity.displayMetrics(): DisplayMetrics {
    val displayMetrics = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics
}
