package ir.mkdev.spotifymusicplayer.extension

import android.view.View
import android.view.animation.AnimationUtils
import ir.mkdev.spotifymusicplayer.R

fun View.setCustomAnimation(currentPosition: Int, lastPosition: Int) {
    if (currentPosition > lastPosition) {
        val animation = AnimationUtils.loadAnimation(context, R.anim.fade_in)
        animation.duration = 700
        this.startAnimation(animation)
    }
}