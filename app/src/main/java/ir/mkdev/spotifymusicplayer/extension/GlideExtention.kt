package ir.mkdev.spotifymusicplayer.extension

import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import ir.mkdev.spotifymusicplayer.R
import ir.mkdev.spotifymusicplayer.utils.GlideRequest


fun <T> GlideRequest<T>.customRequest(
        placeholder: Int = R.color.md_white_1000,
        error: Int = R.color.md_red_500,
        diskCacheStrategy: DiskCacheStrategy = DiskCacheStrategy.AUTOMATIC,
        priority: Priority = Priority.HIGH
): GlideRequest<T> {

    val options = RequestOptions()
            .placeholder(placeholder)
            .error(error)
            .diskCacheStrategy(diskCacheStrategy)
            .priority(priority)
    return apply(options)
}