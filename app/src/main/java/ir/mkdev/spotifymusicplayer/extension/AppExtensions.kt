package ir.mkdev.spotifymusicplayer.extension

import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast
import timber.log.Timber

fun Context.toast(msg: String, duration: Int = Toast.LENGTH_SHORT): Toast {
    return Toast.makeText(this, msg, duration).apply { show() }
}

fun Context.isNetworkAvailable(): Boolean {
    return try {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        networkInfo != null && networkInfo.isConnected
    } catch (e: NullPointerException) {
        Timber.d(e)
        false
    }
}