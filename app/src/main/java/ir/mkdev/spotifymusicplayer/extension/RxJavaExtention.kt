package ir.mkdev.spotifymusicplayer.extension

import android.content.Context
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ir.mkdev.shootballi.callback.ResultCallback
import ir.mkdev.spotifymusicplayer.api.getRetrofitErrorMessage
import ir.mkdev.spotifymusicplayer.models.network.ErrorHandlingModel
import ir.mkdev.spotifymusicplayer.utils.RxUtils
import java.util.concurrent.Callable
import java.util.concurrent.TimeUnit

fun <T> schedulers(): FlowableTransformer<T, T> = FlowableTransformer {
    it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun <T> Flowable<T>.iomain(): Flowable<T> = this.compose(schedulers())
fun <T> Single<T>.iomain(): Single<T> = this.compose {
    it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun Completable.iomain(): Completable = this.compose {
    it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun <T> Maybe<T>.iomain(): Maybe<T> = this.compose {
    it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun <T> Flowable<T>.logError(context: Context,
                             callback: ResultCallback)
        : Flowable<T> = this.compose {

    it.doOnError { throwable ->
        handleError(context, throwable, callback)
    }
}

fun <T> Single<T>.logError(context: Context,
                           callback: ResultCallback)
        : Single<T> = this.compose {

    it.doOnError { throwable ->
        handleError(context, throwable, callback)
    }
}

fun <T> Maybe<T>.logError(context: Context,
                          callback: ResultCallback)
        : Maybe<T> = this.compose {

    it.doOnError { throwable ->
        handleError(context, throwable, callback)
    }
}

fun Completable.logError(context: Context,
                         callback: ResultCallback)
        : Completable = this.compose {

    it.doOnError { throwable ->
        handleError(context, throwable, callback)
    }
}

fun handleError(context: Context, it: Throwable,
                callback: ResultCallback) {
    val message: String? = (getRetrofitErrorMessage(context, it) as ErrorHandlingModel).message
    callback.onResult(message)
}

fun workOnMainThread(block: () -> Unit, onError: (() -> Unit)? = null): Disposable {

    return RxUtils.completable(Callable {
        block.invoke()
        true
    }, onError, AndroidSchedulers.mainThread())
}

fun workOnBackgroundThread(block: () -> Unit, onError: (() -> Unit)? = null): Disposable {

    return RxUtils.completable(Callable {
        block.invoke()
        true
    }, onError, Schedulers.io())
}

fun delayOnMainThread(block: () -> Unit, delay: Long, timeUnit: TimeUnit = TimeUnit.MILLISECONDS): Disposable {

    return RxUtils.delayCompletable(Callable {
        block.invoke()
        true
    }, delay, timeUnit, AndroidSchedulers.mainThread())
}