package ir.mkdev.spotifymusicplayer.enums

enum class ItemViewType {
    ITEM,
    HEADER,
    EMPTY,
    LOADING
}

enum class ListAction {
    ADD,
    REMOVE,
    UPDATE,
    RESET,
    ADD_RANGE
}