# Android - Spotify + Model View ViewModel (MVVM)
This repository was created using MVVM pattern with Kotlin and RxJava, a simple client for [The Music DB](https://developer.spotify.com/) API.

# APK file
[Download Spotify Android Test App File](https://github.com/msddev/Spotify-Android/releases/download/v1.0.0/Spotify-v1.0.0.apk)

# Libraries used on the project
------------------------------------
* [MVVM Architecture](https://developer.android.com/jetpack/docs/guide)
* [Kotlin](https://developer.android.com/kotlin)
* [RxJava 2 & RxAndroid](https://github.com/ReactiveX/RxJava)
* [Android Architecture componenets](https://developer.android.com/topic/libraries/architecture/)
* [Dagger 2](https://google.github.io/dagger/)
* [Retrofit 2](https://square.github.io/retrofit/)
* [Material design 2](https://material.io/develop/android/docs/getting-started/)
* [ConstraintLayout(guidelines, barriers... etc)](https://developer.android.com/training/constraint-layout)
* [Glide](https://bumptech.github.io/glide/)
* [Timber](https://github.com/JakeWharton/timber)


# Preview
![](./screenshot/Screenshot_1565894339.png)
![](./screenshot/Screenshot_1565894397.png)
![](./screenshot/Screenshot_1565894413.png)
![](./screenshot/Screenshot_1565894419.png)
